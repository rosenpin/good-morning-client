package io.rosenpin.goodmorning

import android.app.IntentService
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.io.File
import java.io.FileOutputStream
import java.lang.ref.WeakReference


class DownloadImage : IntentService("DownloadImage") {

  private val NotificationChannelID = "download_image"

  override fun onCreate() {
    super.onCreate()
    val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.createNotificationChannel(
      NotificationChannel(
        NotificationChannelID,
        NotificationChannelID,
        NotificationManager.IMPORTANCE_DEFAULT
      )
    )
    startForeground(123, Notification.Builder(applicationContext, NotificationChannelID).build())
  }

  override fun onHandleIntent(intent: Intent?) {
    downloadImage()
  }

  class TargetPhoneGallery(
    r: ContentResolver?,
    c: Context?,
    private val name: String,
    private val desc: String?
  ) : Target {
    private val resolver: WeakReference<ContentResolver?> = WeakReference<ContentResolver?>(r)
    private val context: WeakReference<Context?> = WeakReference<Context?>(c)


    override fun onPrepareLoad(arg0: Drawable?) {}

    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
    }

    override fun onBitmapLoaded(bitmap: Bitmap?, arg1: Picasso.LoadedFrom?) {
      val filePath =
        Environment.getExternalStoragePublicDirectory(
          Environment.DIRECTORY_PICTURES + "/GoodMorning"
        ).absolutePath
      Toast.makeText(this.context.get(), filePath, Toast.LENGTH_LONG).show()
      val dir = File(filePath)
      if (!dir.exists()) dir.mkdirs()
      val file = File(dir, name)
      val fOut = FileOutputStream(file)

      bitmap?.compress(Bitmap.CompressFormat.PNG, 85, fOut)
      fOut.flush()
      fOut.close()
      galleryAddPic(file)
    }

    private fun galleryAddPic(f: File) {
      val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)

      val contentUri = Uri.fromFile(f)
      mediaScanIntent.data = contentUri
      this.context.get()?.sendBroadcast(mediaScanIntent)
    }
  }

  /**
   * Handle action Foo in the provided background thread with the provided
   * parameters.
   */
  private fun downloadImage() {
    val mainHandler = Handler(Looper.getMainLooper())
    mainHandler.post(Runnable {
      Picasso.get().load("https://image.rosenpin.io")
        .into(TargetPhoneGallery(contentResolver, this, "good-morning.png", "good-morning"))
    })
  }

  companion object {
    @JvmStatic
    fun start(context: Context) {
      val intent = Intent(context, DownloadImage::class.java)
      context.startForegroundService(intent)
    }
  }
}
