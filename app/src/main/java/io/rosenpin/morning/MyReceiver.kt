package io.rosenpin.goodmorning

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast

class MyReceiver : BroadcastReceiver() {

  override fun onReceive(context: Context, intent: Intent) {
    Toast.makeText(context,"triggered",Toast.LENGTH_LONG).show()
    Log.d("triggered","receiver")
    DownloadImage.start(context)
  }
}
