package io.rosenpin.morning

import android.Manifest
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.bumptech.glide.signature.ObjectKey
import com.squareup.picasso.Picasso
import io.rosenpin.goodmorning.DownloadImage


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                1
            )
            return
        }

        window.addFlags(
            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        )


        DownloadImage.start(this@MainActivity)
        Picasso.get().load("https://image.rosenpin.io").into(findViewById<ImageView>(R.id.image))

        // save image to gallery
//        Picasso.get().load("https://image.rosenpin.io")
//            .into(TargetPhoneGallery(contentResolver, this, "good-morning.png", "good-morning"))

        findViewById<Button>(R.id.share).setOnClickListener { shareBitmap() }
        findViewById<Button>(R.id.change).setOnClickListener { requestChange("https://image.rosenpin.io/change") }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    private fun requestChange(url: String?) {
        AlertDialog.Builder(this).setMessage("Are you sure you want to reload image?")
            .setNegativeButton(
                "no"
            ) { dialog, _ -> dialog.cancel() }
            .setPositiveButton("yes") { _, _ ->
                val pd = ProgressDialog.show(this, "Loading", "waiting for server response")
                val queue = Volley.newRequestQueue(this)
                val stringRequest = StringRequest(url,
                    { response: String ->
                        Toast.makeText(
                            applicationContext,
                            response,
                            Toast.LENGTH_LONG
                        ).show()
                        Picasso.get().invalidate("https://image.roesnpin.io")
                        finish()
                        startActivity(Intent(this, MainActivity::class.java))
                    },
                    { error: VolleyError ->
                        Toast.makeText(
                            applicationContext,
                            "Error changing image ${error.networkResponse.statusCode}",
                            Toast.LENGTH_LONG
                        ).show()
                        pd.cancel()
                    }
                )
                queue.add(stringRequest)
            }.show()
    }

    fun shareOnWhatsapp(context: Context, uri: Uri?) {
        val intent = Intent()
        intent.action = "android.intent.action.SEND"
        intent.setPackage("com.whatsapp")
        val stringBuilder = "☀️"
        intent.putExtra("android.intent.extra.TEXT", stringBuilder)
        intent.putExtra("android.intent.extra.STREAM", uri)
        intent.putExtra("jid", "1577048272@broadcast")
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        try {
            context.startActivity(intent)
        } catch (unused: java.lang.Exception) {
            Toast.makeText(context, "whatsapp not installed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun shareBitmap() {
        Glide.with(this).asBitmap()
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .signature(ObjectKey(System.currentTimeMillis().toString()))
            .skipMemoryCache(true)
            .load("https://image.rosenpin.io")
            .into(object : CustomTarget<Bitmap?>() {
                override fun onResourceReady(
                    resource: Bitmap,
                    transition: Transition<in Bitmap?>?
                ) {
                    shareOnWhatsapp(this@MainActivity, getImageUri(resource))
                }

                override fun onLoadCleared(placeholder: Drawable?) {}
            })
    }

    private fun getImageUri(bitmap: Bitmap): Uri? {
        val filename = "good-morning.jpg${System.currentTimeMillis()}"
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
            put(MediaStore.Images.Media.IS_PENDING, 1)
        }

        val contentResolver = application.contentResolver
        val imageUri =
            contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
        val fos = imageUri?.let { contentResolver.openOutputStream(it) }

        fos?.use { bitmap.compress(Bitmap.CompressFormat.JPEG, 90, it) }

        contentValues.clear()
        contentValues.put(MediaStore.Images.Media.IS_PENDING, 0)
        imageUri?.let {
            grantUriPermission(
                this.packageName,
                it,
                FLAG_GRANT_WRITE_URI_PERMISSION
            )
            contentResolver.update(it, contentValues, null, null)
        }

        return imageUri
    }
}